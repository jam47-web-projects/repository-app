import React from 'react';
import Repo from './Repo.js';

const Repos = ({repos, addTag}) => {
    return (
        <>
            {repos.map(repo => (<Repo key={repo.id.toString()} repo={repo} addTag={addTag} />))}
        </>
    );
}

export default Repos;
