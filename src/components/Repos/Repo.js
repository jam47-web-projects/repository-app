import React from 'react';
import './Repo.css';

import Chip from '@material-ui/core/Chip';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import OpenInNew from '@material-ui/icons/OpenInNew';

const Repo = function({repo, addTag}) {
    return (
        <Card variant="outlined" className="Repo">
            <Typography variant="h6">{repo.name}</Typography>
            <a href={repo.web_url} target="_blank" rel="noopener noreferrer" style={{"textDecoration": "none"}}>
                <IconButton aria-label="delete">
                    <OpenInNew />
                </IconButton>
            </a>
            &nbsp;
            {repo.tag_list.map((tag,index) =>
                <> 
                <Chip key={`${repo.id}${index}`} onClick={() => addTag(tag)} color="secondary" size="small" label={tag} /> 
                &nbsp;
                </>)}
            <Typography className="Repo-Description">
                {repo.description}
            </Typography>
        </Card>
    );
}
export default Repo;
