import React from 'react';
import './App.css';
import Repos from '../Repos';

import axios from 'axios';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip'
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';
import AlertTitle from '@material-ui/lab/AlertTitle';
import { ThemeProvider } from '@material-ui/core/styles';
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    secondary: {main: '#0394fc'},
  },
});

const reposReducer = (state, action) => {
  switch (action.type) {
    case 'REPOS_FETCH_INIT':
      return {
        ...state,
        isLoading: true,
        isError: false,
      };
    case 'REPOS_FETCH_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.payload
      };
    case 'REPOS_FETCH_FAILURE':
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    default:
      throw new Error();
  }
};


function App() {
  const API_ENDPOINT = `https://gitlab.com/api/v4/users/2983467/starred_projects?per_page=100`
  const [repos, dispatchRepos] = React.useReducer(
    reposReducer,
    { data: [], isLoading: false, isError: false }
  );

  const handleFetchRepos = React.useCallback(async () => {
    dispatchRepos({ type: 'REPOS_FETCH_INIT' });

    try {
      const result = await axios.get(API_ENDPOINT);
      dispatchRepos({
        type: 'REPOS_FETCH_SUCCESS',
        payload: result.data
      });
    } catch {
      dispatchRepos({ type: 'REPOS_FETCH_FAILURE' });
    }
  }, [API_ENDPOINT]);

  React.useEffect(() => {
    handleFetchRepos();
  }, [handleFetchRepos]);

  const [tags, setTags] = React.useState([]);

  const addTag = tag => {
    console.log(tags.includes(tag));
    if (!tags.includes(tag)) {
      setTags(tags.concat(tag));
    }
  }

  const removeTag = tag => {
    setTags(tags.filter(element => element !== tag));
  }

  return (
    <div>
      <ThemeProvider theme={theme}>
      <AppBar className="App-Header" position="static">
        <Toolbar>
          <Typography variant="h4" className="App-Header">
            My Projects
          </Typography>
        </Toolbar>
      </AppBar>
      <div className="content">
        {repos.isError && <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          Something went wrong!
          </Alert>}
        {tags.map(tag => <Chip color="secondary" size="small" onDelete={() => removeTag(tag)} label={tag} />)}
        <Repos repos={repos.data.filter(repo => tags.length === 0 || tags.every(tag => repo.tag_list.includes(tag)))} addTag={addTag}/>
        {repos.isLoading && <div style={{display: 'flex', justifyContent: 'center'}}><CircularProgress /></div>}
      </div>
      </ThemeProvider>
    </div>
  );
}

export default App;
